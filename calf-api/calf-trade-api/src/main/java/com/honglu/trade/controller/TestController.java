package com.honglu.trade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.honglu.account.entity.User;
import com.honglu.entity.BaseResult;
import com.honglu.trade.service.TestService;
import com.honglu.utils.ResultUtils;

@RestController
public class TestController {

	@Autowired
	private TestService testService;
	
	@GetMapping("/trade/user")
	public BaseResult<User> getUser() {
		User user = testService.getUserByUsername("张三"); 
		return ResultUtils.resultSuccess(user);
	}
}
