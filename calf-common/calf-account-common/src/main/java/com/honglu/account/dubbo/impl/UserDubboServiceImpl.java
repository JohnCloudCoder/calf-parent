package com.honglu.account.dubbo.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.honglu.account.dubbo.UserDubboService;
import com.honglu.account.entity.User;
import com.honglu.account.service.UserService;

//注册为 Dubbo 服务
@Service(version = "1.0.0")
public class UserDubboServiceImpl implements UserDubboService {

	@Autowired
	private UserService userService;
	
	@Override
	public User getUserByUsername(String username) {
		return userService.findByUserame(username);
	}

}
