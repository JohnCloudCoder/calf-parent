package com.honglu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.honglu.account.service.ScheduleJobService;
import com.honglu.account.service.impl.ScheduleJobServiceImpl;

/**
 * Spring Boot 应用启动类
 * @author zhangze
 */
//Spring Boot 应用的标识
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.honglu"})
public class AccountApplication {

	public static void main(String[] args) {
		// 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
		ConfigurableApplicationContext run = SpringApplication.run(AccountApplication.class, args);
		ScheduleJobService scheduleJobService = run.getBean(ScheduleJobServiceImpl.class);
		scheduleJobService.initJob(null);
	}
	
}