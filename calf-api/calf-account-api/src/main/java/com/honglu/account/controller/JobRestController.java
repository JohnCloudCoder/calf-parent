package com.honglu.account.controller;

import java.lang.reflect.Method;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.quartz.CronScheduleBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.honglu.account.common.enums.CalfResultEnum;
import com.honglu.account.entity.ScheduleJob;
import com.honglu.account.service.ScheduleJobService;
import com.honglu.account.utils.SpringIocUtils;
import com.honglu.entity.BaseResult;
import com.honglu.exception.CalfException;
import com.honglu.utils.ResultUtils;

/**
 * JOB操作controller
 * @author zhangze
 *
 */
@RestController
public class JobRestController {
	@Autowired
	private ScheduleJobService scheduleJobService;
	
	/**
	 * 任务列表
	 * @param request
	 * @return
	 */
	@GetMapping("/api/quartz/jobs")
	public BaseResult jobList(@RequestParam(value = "jobGroup", required = false) String jobGroup) {
		ScheduleJob job = new ScheduleJob();
		job.setJobGroup(jobGroup);
		List<ScheduleJob> listJob = scheduleJobService.listByJob4DB(job);
		return ResultUtils.resultSuccess(listJob);
	}

	/**
	 * 新增任务
	 * @param request
	 * @param scheduleJob
	 * @return
	 */
	@PostMapping("/api/quartz/job")
	public BaseResult saveJob(ScheduleJob scheduleJob) {
		try {
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(scheduleJob.getCronExpression());
		} catch (Exception e) {
			return ResultUtils.resultFail(new CalfException(CalfResultEnum.FAIL_SCHEDULE_CRON.getCode(), CalfResultEnum.FAIL_SCHEDULE_CRON.getMessage()));
		}
		Object obj = null;
		try {
			if (StringUtils.isNotBlank(scheduleJob.getSpringId())) {
				obj = SpringIocUtils.getBean(scheduleJob.getSpringId());
			} else {
				Class clazz = Class.forName(scheduleJob.getBeanClass());
				obj = clazz.newInstance();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj == null) {
			return ResultUtils.resultFail(new CalfException(CalfResultEnum.FAIL_OBJECT_NULL.getCode(), CalfResultEnum.FAIL_OBJECT_NULL.getMessage()));
		} else {
			Class clazz = obj.getClass();
			Method method = null;
			try {
				method = clazz.getMethod(scheduleJob.getMethodName(), null);
			} catch (Exception e) {
				// do nothing.....
			}
			if (method == null) {
				return ResultUtils.resultFail(new CalfException(CalfResultEnum.FAIL_OBJECT_NULL.getCode(), CalfResultEnum.FAIL_OBJECT_NULL.getMessage()));
			}
		}
		try {
			scheduleJobService.saveJob2DB(scheduleJob);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultUtils.resultError();
		}
		return ResultUtils.resultSuccess();
	}

	/**
	 * 改变任务状态
	 * @param request
	 * @param jobId
	 * @param cmd
	 * @return
	 */
	@PostMapping("/api/quartz/changeJobStatus")
	public BaseResult changeJobStatus(String jobId, String cmd) {
		try {
			scheduleJobService.changeStatus(jobId, cmd);
		} catch (CalfException e) {
			e.printStackTrace();
			return ResultUtils.resultFail(e);
		}
		return ResultUtils.resultSuccess();
	}

	/**
	 * 更新任务cron表达式
	 * @param request
	 * @param jobId
	 * @param cron
	 * @return
	 */
	@RequestMapping("/api/quartz/updateCron")
	public BaseResult updateCron(String jobId, String cron) {
		try {
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cron);
		} catch (Exception e) {
			return ResultUtils.resultFail(new CalfException(CalfResultEnum.FAIL_SCHEDULE_CRON.getCode(), CalfResultEnum.FAIL_SCHEDULE_CRON.getMessage()));
		}
		try {
			scheduleJobService.updateCron(jobId, cron);
		} catch (CalfException e) {
			e.printStackTrace();
			return ResultUtils.resultFail(e);
		}
		return ResultUtils.resultSuccess();
	}
}
