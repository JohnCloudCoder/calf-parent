package com.honglu.account.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.alibaba.dubbo.common.utils.StringUtils;



/**
 * 读取config.properties配置
 * 
 * @author gdl
 *
 */
public class PropertiesUtil {

	private static Properties properties = new Properties();
	static {
		InputStream in = PropertiesUtil.class.getResourceAsStream("/config.properties");
		try {
			properties.load(in);
		} catch (IOException e) {

		}
	}

	public static String get(String key) {
		String value = properties.getProperty(key);
		if(StringUtils.isNotEmpty(value)){
			return value.trim();
		}
		return null;
	}
}
