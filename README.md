1. 项目骨架：
	-calf-parent 顶级项目层级(项目通用jar包依赖添加在该项目的pom.xml中)
			||
			|| ---------calf-api 二级项目层级(对外提供服务层级的项目管理目录)
			|				| 
			|				| -----------calf-account-api
			|				| -----------calf-trade-api
			||
			|| ---------calf-common 二级项目层级(api 项目引用的xxx-common.jar的源码，其中calf-util-common所有的api及common都需要引用)
			|				| 
			|				| -----------calf-util-common 全局通用common
			|				| -----------calf-account-common
			|				| -----------calf-trade-common
							
2. 项目基础框架: 		SpringBoot
	   ORM框架: 			Mybatis
	   分词搜索框架：		Solr
	   缓存：				Redis
	   RPC框架：			Dubbo
	   服务注册：			ZooKeeper
	   作业调度框架:		Quartz
	   消息队列框架：		ActiveMQ
	  	

3. 示例项目为calf-account-api
	3.1 右击com.honglu目录下的AccountApplication, Run As ---> Java Application
	3.2 SpringBoot默认启动端口为8080,该项目端口为9080, 可自行在application.properties修改端口
	3.3 项目启动成功在浏览器中访问： http://localhost:9080/account/api/user?username=张三
		{"code":"10000","message":"成功","data":{"userId":1,"username":"张三","pwd":"12345","email":"happyle@happyle.com"}}
	3.4 redis示例：redis设置值	http://localhost:9080/account/api/redis/set?username=张三
				   redis获取值	http://localhost:9080/account/api/redis/set?username=张三
	3.5 quartz示例: 获取job列表 	http://localhost:9080/account/api/quartz/jobs?jobGroup=demo
			项目启动会按条件加载需要启动的job
	
4. API 接口返回值格式为：
	{
	    "code":"10000",		// 统一返回值  10000为成功 | 99999为系统内部异常，需要开发人员自行定位问题原因 | 10001-99998 为自定义异常，可根据具体业务逻辑自行定义
	    "message":"成功",	// 统一返回值描述
	    "data":{
	        "userId":1,
	        "username":"张三",
	        "pwd":"12345",
	        "email":"happyle@happyle.com"
	    }
	}
		