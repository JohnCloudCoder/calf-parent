package com.honglu.utils;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.honglu.enums.BaseResultEnum;
import com.honglu.entity.BaseResult;
import com.honglu.exception.CalfException;

public class ResultUtils {

	private ResultUtils() {}
	
	/**
	 * 成功返回
	 * @return
	 */
	public static BaseResult resultSuccess() {
		return resultSuccess(null);
	}
	
	/**
	 * 成功返回
	 * @param data
	 * 			接口返回数据
	 * @return
	 */
	public static BaseResult resultSuccess(Object data) {
		BaseResult result = new BaseResult();
		result.setCode(BaseResultEnum.SUCCESS.getCode());
		result.setMessage(BaseResultEnum.SUCCESS.getMessage());
		result.setData(data);
		return result;
	}
	
	/**
	 * 失败返回
	 * @param e
	 * 			自定义异常
	 * @return
	 */
	public static BaseResult resultFail(CalfException e) {
		BaseResult result = new BaseResult();
		result.setCode(e.getCode());
		result.setMessage(e.getMessage());
		return result;
	}
	
	/**
	 * 错误返回
	 * @return
	 */
	public static BaseResult resultError() {
		BaseResult result = new BaseResult();
		result.setCode(BaseResultEnum.ERROR.getCode());
		result.setMessage(BaseResultEnum.ERROR.getMessage());
		return result;
	}
}
