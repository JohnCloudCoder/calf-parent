package com.honglu.utils;


/**
* 
* @author gdl
* @version 1.0
*/
public enum CodeUtils {
	
	SUCCESS("200","成功",true),
	PARAMS_FAILUE("100","参数无效",false),
	PHONE_NUMBER_NULL("1001","手机号码不能为空",false),
	PHONE_NUMBER_USE("200","该手机号码可以使用",true),
	PHONE_NUMBER_EXIST("1002","该手机号码已存在",false),
	PHONE_NUMBER_ERROR("1003","请输入正确手机号码",false),
	SMS_SEND_SUCCESS("200","短信发送成功，请查收",true),
	SMS_VALIDATE_CODE_ERROR("1004","短信验证码错误",false),
	SMS_VALIDATE_SUCCESS("200","短信验证码校验成功",true),
	GET_VALIDATE_CODE("1005","请重新获取短信验证码",false),
	LOGIN_SUCCESS("200","登录成功",true),
	LOGIN_FAILUE("1006","登录失败",false),
	SYSTEM_BUSY("1007","系统繁忙，请稍后再试",false),
	VALIDATE_CODE_NULL("1008","短信验证码不能为空",false),
	DATAERROR("3001","日期不能为空",false),
	DATAPATTERNERROR("3002","日期格式不正确，请重新输入。",false),
	ERROR("3003","失败。",false),	
	ERRORDATANULL("3004","没有第0页。",false),
	SUCCESSNEWS("3005","返回成功。",true),
	PAGEERROR("3006","失败。",false),
	NO_LOGIN("1009","请登录",false),
	NO_COUPON_ID("1009","优惠券编号为空",false),
	NO_COUPON_TYPE_ID("1010","优惠券类型为空",false),
	NO_INVEST_PRODUCT_ID("1011","投资产品编号",false),
	NO_USE_ID("1011","使用编号不能为空",false),
	NO_PROFIT_ID("1011","止盈止损编号",false),
	KLINE_TYPE("1012","K线类型不能为空",false),
	QUERY_QUOTE_FAILUE("1013","获取最新行情数据失败",false),
	KLINE_GOODTYPE("1014","商品类型不能为空",false),
	BOURSE_GOODTYPE("1015","交易所编号不能为空",false),
	DATE_NODE("1016","时间节点不能为空",false),
	SYSTEM_ERROR("500","服务器异常",false),
	GG_ERROR("10000","广贵返回产品ID空",false),
	TOKEN_ERROR("10001","令牌失效",false),
	NULL_ERROR("10002","空",false);
	
	
	private String code;//错误码
	private String message;//错误消息
	private Boolean flag;//返回状态
	
	private CodeUtils(String code, String message,Boolean flag) {
		this.code = code;
		this.message = message;
		this.flag = flag;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	
}
