package com.honglu.account.utils;

import com.honglu.account.common.enums.CalfResultEnum;
import com.honglu.exception.CalfException;

public class ExceptionUtils {

	private ExceptionUtils() {}
	
	public static void initCalfException(CalfResultEnum resultEnum) {
		throw new CalfException(resultEnum.getCode(), resultEnum.getMessage());
	}
	
	public static void initCalfException(String code, String message) {
		throw new CalfException(code, message);
	}
}
