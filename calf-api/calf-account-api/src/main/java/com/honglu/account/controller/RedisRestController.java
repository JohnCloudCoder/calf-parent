package com.honglu.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.honglu.account.service.RedisService;
import com.honglu.entity.BaseResult;
import com.honglu.utils.ResultUtils;

/**
 * Redis操作controller
 * @author zhangze
 *
 */
@RestController
public class RedisRestController {	

    @Autowired
    private RedisService redisService;
    

    /**
     * 设置redis
     * @param username
     * @return
     */
    @RequestMapping(value = "/api/redis/set", method = RequestMethod.GET)
    public BaseResult<String> setRedis(@RequestParam(value = "username", required = true) String username) {
    	redisService.setRedis(username);
        return ResultUtils.resultSuccess();
    }
    
    /**
     * 查询redis
     * @return
     */
    @RequestMapping(value = "/api/redis/get", method = RequestMethod.GET)
    public BaseResult<String> getRedis(@RequestParam(value = "username", required = true) String username) {
    	String result = redisService.getRedis(username);
        return ResultUtils.resultSuccess(result);
    }

}