package com.honglu.account.common.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;
import com.honglu.account.entity.ScheduleJob;
import com.honglu.account.utils.TaskUtils;



/**
 * 
 * 计划任务执行工厂类(无状态)
 * @author zhangze
 */
@Component
public class QuartzJobFactory implements Job {
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		ScheduleJob scheduleJob = (ScheduleJob) context.getMergedJobDataMap().get("scheduleJob");
		TaskUtils.invokMethod(scheduleJob);
	}
}