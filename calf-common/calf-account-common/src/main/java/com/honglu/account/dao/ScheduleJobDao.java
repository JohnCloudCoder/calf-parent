package com.honglu.account.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.honglu.account.entity.ScheduleJob;

@Mapper
public interface ScheduleJobDao {
	
	/**
	 * 插入任务信息
	 * @param job
	 * @return
	 */
	int saveJob(ScheduleJob job);
	
	/**
	 * 更新任务信息
	 * @param job
	 * @return
	 */
	int updateByJob(ScheduleJob job);
	
	/**
	 * 根据任务编号删除任务
	 * @param jobId
	 * @return
	 */
	int deleteByJobId(String jobId);

	/**
	 * 根据任务编号查询任务详情
	 * @param jobId
	 * @return
	 */
	ScheduleJob getByJobId(String jobId);
	
	/**
	 * 根据条件查询任务信息列表
	 * @return
	 */
	List<ScheduleJob> listByJob(ScheduleJob job);

	/**
	 * 获取所有的任务信息列表
	 * @return
	 */
	List<ScheduleJob> listAll();
}