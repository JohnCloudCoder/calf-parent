package com.honglu.trade.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.honglu.account.dubbo.UserDubboConsumerServiceImpl;
import com.honglu.account.entity.User;
import com.honglu.trade.service.TestService;

@Component
public class TestServiceImpl implements TestService {

	@Autowired
	UserDubboConsumerServiceImpl userDubboConsumerServiceImpl;
	
	public User getUserByUsername(String username) {
		return userDubboConsumerServiceImpl.getUserByUsername(username);
	}

}
