package com.honglu.account.dubbo;

import org.springframework.stereotype.Component;
import com.alibaba.dubbo.config.annotation.Reference;
import com.honglu.account.entity.User;

@Component
public class UserDubboConsumerServiceImpl {

	@Reference(version = "1.0.0")
	UserDubboService userDubboService;
	
	public User getUserByUsername(String username) {
		return userDubboService.getUserByUsername(username);
	}

}
