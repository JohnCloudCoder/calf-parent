package com.honglu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.honglu.account.dubbo.UserDubboConsumerServiceImpl;


@EnableAutoConfiguration
@ComponentScan(basePackages={"com.honglu"})
public class TradeApplication {

	public static void main(String[] args) {
		// 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
		ConfigurableApplicationContext run = SpringApplication.run(TradeApplication.class, args);
//		UserDubboConsumerServiceImpl userService = run.getBean(UserDubboConsumerServiceImpl.class);
//		System.out.println(userService.getUserByUsername("张三").getUserId());
	}
	
}