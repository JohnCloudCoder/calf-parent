package com.honglu.account.service;

import java.util.List;

import org.quartz.SchedulerException;
import com.honglu.account.entity.ScheduleJob;
import com.honglu.exception.CalfException;

public interface ScheduleJobService {
	
	/**
	 * 项目启动初始化job
	 */
	public void initJob(ScheduleJob paramJob);
	
	/**
	 * 从数据库中取 根据条件匹配job
	 * @return
	 */
	public List<ScheduleJob> listByJob4DB(ScheduleJob job);
	
	/**
	 * 从数据库中查询job
	 */
	public ScheduleJob getByJobId4DB(String jobId);
	
	/**
	 * 新增job到数据库中
	 */
	public void saveJob2DB(ScheduleJob job);

	/**
	 * 更改任务状态(同步更新数据库和调度器)
	 * @throws SchedulerException
	 */
	public void changeStatus(String jobId, String cmd) throws CalfException;

	/**
	 * 更改任务 cron表达式
	 * 
	 * @throws SchedulerException
	 */
	public void updateCron(String jobId, String cron) throws CalfException;

}
