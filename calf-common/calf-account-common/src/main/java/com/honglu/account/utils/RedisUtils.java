
package com.honglu.account.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * redis操作工具类
 * @author zhangze
 *
 */
@Component
public class RedisUtils {

    @Autowired
    JedisPool jedisPool;

    public Jedis getResource() {
        return jedisPool.getResource();
    }

    @SuppressWarnings("deprecation")
    public void returnResource(Jedis jedis) {
        if (jedis != null) {
            jedisPool.returnResourceObject(jedis);
        }
    }
    
    @SuppressWarnings("deprecation")
    public void returnBrokenResource(Jedis jedis) {
    	if (jedis != null) {
            jedisPool.returnBrokenResource(jedis);
        }
    }

    /**
	 * 删除key
	 * @param key 	于Redis中的key
	 */
	public void del(String key) {
		Jedis jedis = null;
		try {
			jedis = getResource();
			// 获取redis中的value
			jedis.del(key);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
	}
	
	/**
	 * 判断key是否存在
	 * @param key 	于Redis中的key
	 */
	public Boolean exists(String key) {
		Jedis jedis = null;
		Boolean flag = false;
		try {
			jedis = getResource();
			// 获取redis中的value
			flag = jedis.exists(key);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return flag;
	}
	
	/**
	 * 判断名称为key的hash中是否存在键为field的域(redis map操作)
	 * @param key 	于Redis中的key
	 * @param field 于Redis Map中的key
	 */
	public Boolean hexists(String key, String field) {
		Jedis jedis = null;
		Boolean flag = false;
		try {
			jedis = getResource();
			// 获取redis中的value
			flag = jedis.hexists(key, field);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return flag;
	}
	
	/**
	 * 将 value 的值写入 Redis Map中(redis map操作)
	 * HSET 将哈希表 key 中的域 field 的值设为 value 。如果 key 不存在，一个新的哈希表被创建并进行 HSET 操作。如果域 field 已经存在于哈希表中，旧值将被覆盖。
	 * 如果 field 是哈希表中的一个新建域，并且值设置成功，返回 1 。如果哈希表中域 field 已经存在且旧值已被新值覆盖，返回 0 。
	 * @param key   于Redis中的key
	 * @param field 于Redis Map中的key
	 * @param value 存储数据
	 * 			
	 */
	public void hset(String key, String field, String value) {
		Jedis jedis = null;
		try {
			jedis = getResource();
			// 获取redis中的value
			jedis.hset(key, field, value);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
	}
	
	/**
	 * 将 value 的值写入 Redis Map中(redis map操作)
	 * HSETNX 将哈希表 key 中的域 field 的值设置为 value ，当且仅当域 field 不存在。若域 field 已经存在，该操作无效。如果 key 不存在，一个新哈希表被创建并执行 HSETNX 命令。
	 * 设置成功，返回 1 。如果给定域已经存在且没有操作被执行，返回 0.
	 * @param key     于Redis中的key
	 * @param field   于Redis Map中的key
	 * @param value   存储数据
	 * @param seconds 失效时间
	 */
	public void hsetEx(String key, String field, String value, int seconds) {
		Jedis jedis = null;
		try {
			jedis = getResource();
			// 获取redis中的value
			jedis.hset(key, field, value);
			jedis.expire(key, seconds);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
	}
	
	 /**
	  * 返回哈希表key中所有域和值(redis map操作)
	  * @param key 于Redis中的key
	  * @return key对应的Map对象
	  */
	public Map<String, String> hgetAll(String key) {
		Jedis jedis = null;
		Map<String, String> map = new HashMap<String, String>();
		try {
			jedis = getResource();
			// 获取redis中的value
			map = jedis.hgetAll(key);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return map;
	}
	
	
	
	/**
	 * 返回名称为key的hash中field对应的value(redis map操作)
	 * @param key 	于Redis中的key
	 * @param field 于Redis Map中的key
	 */
	public String hget(String key, String field) {
		Jedis jedis = null;
		String value = "";
		try {
			jedis = getResource();
			// 获取redis中的value
			value = jedis.hget(key, field);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return value;
	}
	
	/**
	 * 返回名称为key的hash中field对应的value(redis map操作)
	 * @param key 	于Redis中的key
	 * @param fields 于Redis Map中的key(可以传入数组)
	 */
	public List<String> hmget(String key, String... fields) {
		Jedis jedis = null;
		List<String> value = null;
		try {
			jedis = getResource();
			// 获取redis中的value
			value = jedis.hmget(key, fields);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return value;
	}
	
	/**
	 * 将数据写入redis List(String 类型的操作)
	 * @param key 	于Redis中的key
	 * @param values 存储数据
	 */
	public void lpush(String key, String... values) {
		Jedis jedis = null;
		try {
			jedis = getResource();
			jedis.lpush(key, values);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
	}
	
	/**
	 * 将数据写入redis List(String 类型的操作)
	 * @param key 		于Redis中的key
	 * @param seconds	有效期时间
	 * @param values 	存储数据
	 */
	public void lpushEx(String key, int seconds, String... values) {
		Jedis jedis = null;
		try {
			jedis = getResource();
			jedis.lpush(key, values);
			jedis.expire(key, seconds);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
	}
	
	/**
	 * 获取redis中 List(String 类型的操作)
	 * @param key 		于Redis中的key
	 * @param start		开始索引
	 * @param end 		结束索引
	 */
	public List<String> lrange(String key, long start, long end) {
		Jedis jedis = null;
		List<String> values = null;
		try {
			jedis = getResource();
			values = jedis.lrange(key, start, end);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return values;
	}
	
	/**
	 * 获取redis中 List指定索引的值(String 类型的操作)
	 * @param key 		于Redis中的key
	 * @param index		索引
	 */
	public String lindex(String key, long index) {
		Jedis jedis = null;
		String value = null;
		try {
			jedis = getResource();
			value = jedis.lindex(key, index);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return value;
	}
	
	/**
	 * 获取redis中 List的长度(String 类型的操作)
	 * @param key 		于Redis中的key
	 */
	public long llen(String key) {
		Jedis jedis = null;
		long length = -1;
		try {
			jedis = getResource();
			length = jedis.llen(key);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return length;
	}
	
	/**
	 * 将数据写入redis(String 类型的操作)
	 * @param key 	于Redis中的key
	 * @param value 存储数据
	 */
	public void set(String key, String value) {
		Jedis jedis = null;
		try {
			jedis = getResource();
			jedis.set(key, value);
		} catch (Throwable e) {
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
	}
	
	/**
	 * 将数据写入redis并设置失效日期(String 类型的操作)
	 * @param key 	     于Redis中的key
	 * @param seconds 超时时间（单位：秒）
	 * @param value	     存储数据
	 */
	public void setEx(String key, int seconds, String value) {
		Jedis jedis = null;
		try {
			jedis = getResource();
			jedis.setex(key, seconds, value);
		} catch (Throwable e) {
			
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
	}
	
	/**
	 * 从redis中读取数据(String 类型的操作)
	 * @param key 	于Redis中的key
	 * @param value 存储数据
	 */
	public String get(String key) {
		Jedis jedis = null;
		String value = "";
		try {
			jedis = getResource();
			// 获取redis中的value
			value = jedis.get(key);
		} catch (Throwable e) {
			
			// 异常情况释放redis对象
			returnBrokenResource(jedis);
			e.printStackTrace();
		}finally{
			// 返还到连接池
			returnResource(jedis);
		}
		return value;
	}

}
