package com.honglu.account.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.honglu.account.common.enums.CalfResultEnum;
import com.honglu.account.dao.UserDao;
import com.honglu.account.entity.User;
import com.honglu.account.service.RedisService;
import com.honglu.account.utils.ExceptionUtils;
import com.honglu.account.utils.RedisUtils;
import com.honglu.exception.CalfException;

@Service
public class RedisServiceImpl implements RedisService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RedisUtils redisUtils;
	
	
	@Override
	public void setRedis(String username) throws CalfException {
		User user = userDao.findByUsername(username);
		if (user == null) {
			ExceptionUtils.initCalfException(CalfResultEnum.FAIL_OBJECT_NULL);
		}
		redisUtils.set(username, user.toString());
	}

	@Override
	public String getRedis(String username) {
		return redisUtils.get(username);
	}

}
