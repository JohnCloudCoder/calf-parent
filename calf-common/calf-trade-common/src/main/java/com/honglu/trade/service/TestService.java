package com.honglu.trade.service;

import com.honglu.account.entity.User;

public interface TestService {
	public User getUserByUsername(String username);
}
