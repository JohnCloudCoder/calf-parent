package com.honglu.account.dubbo;

import com.honglu.account.entity.User;

public interface UserDubboService {
	/**
	 * 通过username获取到用户 信息
	 * @param username
	 * @return
	 */
	public User getUserByUsername(String username);
}
